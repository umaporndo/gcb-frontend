/**
 * @namespace
 * @desc Handles file upload management.
 */
const FileUpload = (function(){
	/**
	 * @memberOf FileUpload
	 * @access public
	 * @desc Initialize FileUpload module.
	 */
	function initialize(){
		$( '.profile-upload' ).click( function(){
			$( '#dialog-upload-image' ).modal( 'show' );
		} );

		var resize = $( '#upload-demo' ).croppie( {
			                                          enableExif:        true,
			                                          enableOrientation: true,
			                                          viewport:          { // Default { width: 100, height: 100, type: 'square' }
				                                          width:  150,
				                                          height: 150,
				                                          type:   'circle', //square
			                                          },
			                                          boundary:          {
				                                          width:  300,
				                                          height: 300,
			                                          },
		                                          } );

		$( '#image' ).on( 'change', function(){
			var reader    = new FileReader();
			reader.onload = function( e ){
				resize.croppie( 'bind', {
					url: e.target.result,
				} ).then( function(){
					console.log( 'jQuery bind complete' );
				} );
			};
			reader.readAsDataURL( this.files[0] );
		} );

		$('.btn-upload-image').on('click', function (ev) {
			var uploadURL = $(this).data('url');
			resize.croppie('result', {
				type: 'canvas',
				size: 'viewport',
			}).then(function (img) {
				$.ajax({
					       url: uploadURL,
					       type: "POST",
					       data: {"profile_image":img},
					       success: function ( result, statusText, jqXHR ) {
						       html = '<img src="' + img + '"/>';
						       $("#preview-crop-image").html(html);
						       $(".hide-input").html('<input type="text" class="d-none" name="image_path" value="'+result.image_path+'">');
					       }
				       });
			});
		});

	}

	return {
		initialize: initialize,
	};
})( jQuery );

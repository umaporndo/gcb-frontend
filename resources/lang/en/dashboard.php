<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dashboard Page Language Lines
    |--------------------------------------------------------------------------
    */

    'page_link' => [
        'index' => "<img src=" . asset('images/menu/dashboard.svg') . " width=20>" . ' Dashboard',
    ],

    'page_title' => [
        'index' => 'Dashboard',
    ],

    'page_description' => [
        'index' => 'Dashboard page',
    ],

    'page_heading' => [
        'index' => 'Dashboard',
    ],


];
<?php
/**
 * Dashboard Page Controller
 */

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Dashboard Page Controller
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Display home page.
     *
     * @return Factory|View Dashboard page
     */
    public function index()
    {
        return view( 'dashboard.index' );
    }
}

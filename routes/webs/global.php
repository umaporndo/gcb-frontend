<?php

use App\Http\Controllers\WebhookController;
use Illuminate\Support\Facades\Route;

/**
 * Global routes
 */
function globalRoutes()
{
    Route::get( 'language/{languageCode}', 'LanguageController@changeLanguage' )->name( 'language.change' );
    Route::get( 'file/{url}', 'MediaController@getFile' )->name( 'getFile' );

    Route::get( 'sitemap', 'SitemapController@index' )->name( 'sitemap.index' );
    Route::get( 'sitemap.xml', 'SitemapController@xml' )->name( 'sitemap.xml' );

    Route::group( [ 'middleware' => 'auth' ], function(){
        //Route::get( '/', 'HomeController@index' )->name( 'home.index' );
        Route::get( 'dashboard', 'DashboardController@index' )->name( 'dashboard.index' );
        Route::get( 'wallet', 'WalletController@index' )->name( 'wallet.index' );
        Route::get( 'project-info', 'ProjectInfoController@index' )->name( 'project-info.index' );
        Route::get( 'project-detail/{id}', 'ProjectInfoController@detail' )->name( 'project-info.detail' );
        Route::get( 'knowledge-base', 'KnowledgeBaseController@index' )->name( 'knowledge-base.index' );
        Route::get( 'knowledge-base/{id}', 'KnowledgeBaseController@detail' )->name( 'knowledge-base.detail' );
        Route::get( 'profile', 'ProfileController@index' )->name( 'profile.index' );
        Route::post( 'profile/{id}', 'ProfileController@updateProfile' )->name( 'profile.update-profile' );
        Route::post( 'upload-profile-image/{id}', 'ProfileController@updateProfileImage' )->name( 'profile.update-profile-image' );
        Route::post( 'upload-profile-image-path', 'ProfileController@updateProfileImagePath' )->name( 'profile.update-profile-image-path' );
        Route::get( 'organization', 'OrganizationController@index' )->name( 'organization.index' );
        Route::get( 'get-company', 'OrganizationController@getcompany' )->name( 'organization.getcompany' );
        Route::post( 'upload-company-image', 'OrganizationController@updateCompanyImage' )->name( 'organization.update-company-image' );
        Route::post( 'upload-company-image-path', 'OrganizationController@updateCompanyImagePath' )->name( 'organization.update-company-image-path' );
        Route::post( 'join-company', 'OrganizationController@requestToJoin' )->name( 'organization.request-to-join' );
        Route::post( 'cancel-join-company', 'OrganizationController@cancelToJoin' )->name( 'organization.cancel-to-join' );
        Route::post( 'action-join-company', 'OrganizationController@actionToJoin' )->name( 'organization.action-to-join' );
        Route::post( 'organization', 'OrganizationController@create' )->name( 'organization.create' );
        Route::post( 'logout', 'Auth\LoginController@logout' )->name( 'logout' );
    } );

    Route::middleware( 'guest' )->group( function(){
        Route::get( '/', 'HomeController@index' )->name( 'home.index' );
        Route::post('passbase-webhooks', [WebhookController::class, 'receive_passbase_webhook']);
        // Authentication
        Route::get( 'login', 'Auth\LoginController@showLoginForm' )->name( 'login' );
        Route::post( 'login', 'Auth\LoginController@login' )->name( 'submitLogin' );
        Route::get( 'login-with-oreid/{email}', 'Auth\LoginController@sendLoginWithOreIDResponse' )->name( 'sendLoginWithOreIDResponse' );

        // Registration
        Route::get( 'signup', 'Auth\RegisterController@showRegistrationForm' )->name( 'register' );
        Route::post( 'signup', 'Auth\RegisterController@register' )->name( 'submitRegister' );

        // Password Reset
        Route::get( 'password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm' )->name( 'password.request' );
        Route::post( 'password/email', 'Auth\ForgotPasswordController@sendResetPasswordLink' )->name( 'password.email' );
        Route::get( 'password/reset/{token}', 'Auth\ResetPasswordController@showResetForm' )->name( 'password.reset' );
        Route::post( 'password/reset', 'Auth\ResetPasswordController@reset' )->name( 'password.change' );
    } );
}
